﻿using GeradorLinhaEnvironmentAws.Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;

namespace GeradorLinhaEnvironmentAws.Mvc.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;

        public HomeController(ILogger<HomeController> logger)
        {
            _logger = logger;
        }

        public IActionResult Index()
        {
            var config = new Configuracoes();
            config.SetarAppSettingsExemplo();

            return View(config);
        }

        [HttpPost]
        public IActionResult Index(Configuracoes configuracoes)
        {
            ModelState.Clear();

            try
            {
                configuracoes.CriarEnvironmentVariablesConvertido();
            }
            catch (Exception ex)
            {
                ModelState.AddModelError(nameof(configuracoes.AppSettings), $"Erro na conversão do Json: {ex.Message}");
            }

            return View(configuracoes);
        }
    }
}