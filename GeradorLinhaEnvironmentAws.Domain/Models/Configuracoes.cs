using System.Collections.Generic;
using System.Linq;
using System.Text;
using GeradorLinhaEnvironmentAws.Domain.Services;
using Newtonsoft.Json.Linq;

namespace GeradorLinhaEnvironmentAws.Domain.Models
{
    public class Configuracoes
    {
        public string AppSettings { get; set; }
        public string EnvironmentVariables { get; set; }

        public void SetarAppSettingsExemplo()
        {
            var linhasGerar = new StringBuilder();
            linhasGerar.AppendLine("{");
            linhasGerar.AppendLine("  \"Aws\": {");
            linhasGerar.AppendLine("    \"ElasticSearch\": {");
            linhasGerar.AppendLine("      \"AwsAccessKeyId\": \"Your_Access_Key_Id\",");
            linhasGerar.AppendLine("      \"AwsSecretAccesKey\": \"Your_Secret_Access_Key\",");
            linhasGerar.AppendLine("      \"Url\": \"Your_Url\",");
            linhasGerar.AppendLine("      \"IndexFormat\": \"Your_Index_Format\"");
            linhasGerar.AppendLine("    }");
            linhasGerar.AppendLine("  }");
            linhasGerar.AppendLine("}");

            AppSettings = linhasGerar.ToString();
            EnvironmentVariables = string.Empty;
        }

        public void CriarEnvironmentVariablesConvertido()
        {
            Dictionary<string, string> nodes = new Dictionary<string, string>();

            JObject rootObject = JObject.Parse(AppSettings);
            new ConversorJsonAws().ParseJson(rootObject, nodes);

            EnvironmentVariables = string.Join(";", nodes.Select(o => $"\\\"{o.Key}\\\"=\\\"{o.Value}\\\""));
        }
    }
}