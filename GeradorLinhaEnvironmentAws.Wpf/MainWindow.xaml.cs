﻿using System.Windows;
using GeradorLinhaEnvironmentAws.Domain.Models;

namespace GeradorLinhaEnvironmentAws.Wpf
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();

            var configuracoes = new Configuracoes();
            configuracoes.SetarAppSettingsExemplo();

            TextAppSettings.Text = configuracoes.AppSettings;
        }

        private void ButtonGerar_Click(object sender, RoutedEventArgs e)
        {
            var configuracoes = new Configuracoes();
            configuracoes.AppSettings = TextAppSettings.Text;

            configuracoes.CriarEnvironmentVariablesConvertido();
            TextEnvironmentVariables.Text = configuracoes.EnvironmentVariables;
        }
    }
}
